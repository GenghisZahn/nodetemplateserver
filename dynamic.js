let fs = require('fs');
let fs2 = require('file');
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let Mustache = require('mustache');

let _files = [];    // {jsFile, templateFile, path}
let _templateDir = 'templates';
let _listenerBasePath = '/dynamic'
let _listenerPort = 3000;
let _templates = {};

/**
 *
 */
function setup() {
  if (!_listenerBasePath.endsWith('/')) {
    _listenerBasePath += '/';
  }
  // walk through the tempate directory and register all templates
  fs2.walk(_templateDir, dirCallback);

  // parse application/x-www-form-urlencoded 
  app.use(bodyParser.urlencoded({ extended: false }));
  
  // parse application/json 
  app.use(bodyParser.json());
  
  // generate an index document listing the available template paths
  app.get(_listenerBasePath, function (req, res) {
    res.set('Content-Type', 'text/html');
    let body = '<!DOCTYPE html><html><head></head><body><ul>\n';
    for (file of _files) {
      body += '<li><a href="'+ file.path +'">'+ file.templateFile +'</a></li>\n';
    }
    body += '</ul></body></html>';
  	res.send(body);
  });

  // process a template request
  app.all(_listenerBasePath + '*', handleRequest);

  app.listen(_listenerPort, function () {
    console.log('Example app listening on port ' + _listenerPort + '!');
  });
}

/**
 *
 */
function handleRequest(req, res) {
  let file = _files.find(f => f.path === req.path);
  if (file === undefined || file === null) {
    // if the path is not in the list of templates, return a 404
    res.status(404).send('File not found');
  }
  else {
    let template = _templates[req.path];
    if (template === undefined || template === null) {
      // read file
      fs.readFile(file.templateFile, 'utf8', function (err, data) {
        if (err) {
          throw err;
        }
        template = data;
        // optional, speeds up future uses
        Mustache.parse(template);
        // store parsed template for later use
        _templates[req.path] = template;
        console.log('Caching template: ' + file.templateFile);
        renderTemplate(template, file, req, res);
      });
    }
    else {
      renderTemplate(template, file, req, res);
    }
  }
}

/**
 *
 */
function renderTemplate(template, file, req, res) {
  let data = {};
  if (file.jsFile !== undefined && file.jsFile !== null) {
    let module = require(file.jsFile);
    data = module.getData(req);
  } else {
    data = req;
  }
  // render template
  var rendered = Mustache.render(template, data);
  let fileExt = file.templateFile.substr(file.templateFile.lastIndexOf('.'));
  res.type(fileExt);
  res.send(rendered);
}

/**
 *
 */
function dirCallback (err, dirPath, dirs, files) {
  if (err !== undefined && err !== null) {
	  console.error(err);
	} else {
    // Array.prototype.push.apply(_files, files);
    for (file of files) {

      // match names like *.template.[extension]
      let matches = file.match(/(.+)\.template\.([^\.]+)/);
      if (matches !== null) {
        // strip out the ".template." portion of the path
        let pathName = matches[1] + '.' + matches[2];
        // replace windows backslashes with web-style forward slashes
        pathName = pathName.replace(/\\/g, '/');
        // remove template directory from path
        pathName = pathName.replace(_templateDir, '');
        // if pathName starts with a /, remove it (it'll be added as part of _listenerBasePath)
        pathName = pathName.replace(/^\//, '');

        let fileObj = {"path": _listenerBasePath + pathName, "templateFile": file};
        // look for a .js file to go with the .template.* file
        let jsFilePath = matches[1];
        if (files.some(f => f === (jsFilePath + '.js'))) {
          // replace windows backslashes with web-style forward slashes
          jsFilePath = jsFilePath.replace(/\\/g, '/');

          fileObj.jsFile = './' + jsFilePath;
        }
        // console.log(fileObj);
        _files.push(fileObj);
      }
    }
	}
}

//===============================================
// Initialize
//===============================================
setup();
